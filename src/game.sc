patterns:
    $Number = $regexp<\d+>

theme: /game

################### Вводная

    state: LetsPlay
        q!: testStartGame
        a: Я умею играть в «Быки и коровы». Напомнить правила или сразу сыграем?
        go!: /game/GameButtons


    state: GameButtons || modal = true
        buttons:
            "Правила"
        buttons:
            "Играем"

        state: react_Rules
            q: * (правило/правила/правилам) *
            go!: /game/GameRules

        state: react_Start
            q: * {[давай*] (играть/играем/сыграть/сыграем/поиграть/поиграем/игра/игру/сразу/да/ага/давай*)} *
            go!: /game/StartGame

        state: react_Stop
            q: * (stop/стоп/хватит/закончи*) *
            a: Не смею задерживать.
            go!: /HowCanIHelp

        state: react_Any
            q: *
            a: Придется выбрать из кнопок.
            go!: /game/GameButtons


################### Начало Игры

    state: GameRules
        a: Я задумываю 4-значное число с неповторяющимися цифрами. Пользователь делает первую попытку отгадать число. Попытка — это 4-значное число с неповторяющимися цифрами. В ответ я сообщу, сколько цифр угадано без совпадения с их позициями в числе (aka коровы) и сколько угадано вплоть до позиции в числе (aka быки).
        a: Например: Задумано число «3219». -> Попытка: «2310». -> Результат: две «коровы» и один «бык».
        a: Играем?
        go: /game/GameButtons


    state: StartGame
        script:
            $client.secretNumber = "";
            var digits = "0123456789";
            // формируем рандомное число
            // удаляем выбранные элементы, чтобы избежать повторов
            for (var i = 0; i < 4; i++) {
                var digit = digits[Math.floor(Math.random() * digits.length)];
                $client.secretNumber += digit;
                digits = digits.replace(digit, "");
            }
        a: Загадала {{$client.secretNumber}}
        a: Нужно ввести 4-значное число с неповторяющимися цифрами (без пробелов между цифрами).


        state: react_Number
            q: * $Number *
            script:
                // приведем пойманное число к строке
                $client.guessedNumber = $parseTree._Number.toString();
                // если не 4-значное
                if ($client.guessedNumber.length !== 4) {
                    $reactions.transition("/game/StartGame/react_Any");
                    return;
                }
                // сюда будем писать цифры из введенного пользователем числа
                $client.match = {};
                for (var i = 0; i < 4; i++) {
                    var symb = $client.guessedNumber[i];
                    // если такая цифра уже была - уходим
                    if ($client.match[symb]) {
                        $client.guessedNumber = undefined;
                        $reactions.transition("/game/StartGame/react_Any");
                        return;
                    }
                    $client.match[symb] = true;
                }
            go!: /game/BullsAnsCowsCount


        state: react_Any
            q: *
            a: Так не пойдет, нужно 4-значное число без повторов и пробелов.
            go: /game/StartGame


        state: react_GiveUp
            q: * (сдаюсь/сдаемся/сдаться) *
            go!: /game/StopGame



    state: BullsAnsCowsCount
        script:
            // если по непонятным причинам нечего сравнивать - уходим
            if (!$client.guessedNumber || !$client.secretNumber) {
                $reactions.transition("/game/Error");
                return;
            }
            // если совпало - победа
            if ($client.guessedNumber === $client.secretNumber) {
                $reactions.transition("/game/WinGame");
                return;
            }
            // остаемся считать животных
            $temp.bulls = 0;
            $temp.cows = 0;
            for (var i = 0; i < 4; i++) {
                if ($client.guessedNumber[i] === $client.secretNumber[i]) {
                    $temp.bulls += 1;
                } else if ($client.match[$client.secretNumber[i]]) {
                    $temp.cows += 1;
                }
            }
        a: Теущий результат: число быков {{$temp.bulls}}, число коров {{$temp.cows}}. Следующая попытка!
        go: /game/StartGame


################### Конец Игры

    state: Error
        a: Что-то пошло не так.
        go!: /HowCanIHelp

    state: StopGame
        a: Закончим!
        if: $client.secretNumber
            go!: /game/RightAnswer

    state: WinGame
        a: Победа!
        if: $client.secretNumber
            go!: /game/RightAnswer

    state: RightAnswer
        a: Загаданное число - {{$client.secretNumber}}.
        script:
            // на всякий сбросим
            $client.secretNumber = undefined;
            $client.guessedNumber = undefined;
