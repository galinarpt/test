require: slotfilling/slotFilling.sc
  module = sys.zb-common

require: game.sc

theme: /

    state: Start
        q!: $regex</start>
        a: Начнем.

    state: Hello
        intent!: /hello
        a: Приветики

    state: Bye
        intent!: /bye
        a: Пока

    state: NoMatch
        event!: noMatch
        a: Не очень понятно, вижу: {{$request.query}}

    state: HowCanIHelp
        a: Чем-то могу помочь?

    state: Stop
        q!: * (stop/стоп/хватит/закончи*) *
        go!: /HowCanIHelp
